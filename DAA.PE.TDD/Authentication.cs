﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DAA.PE.TDD
{
    public static class Authentication
    {
        private static readonly string Path = Directory.GetCurrentDirectory() + "\\users.xml";
        public static List<User> Users = new List<User>();

        public static bool CreateUser(string username, string password)
        {
            var xmlSer = new XmlSerializer(typeof(List<User>));
            if (!File.Exists(Path))
            {
                var doc = new XDocument(new XElement("ArrayOfUser"));
                doc.Save("users.xml");
            }
            using (var xr = XmlReader.Create(Path))
            {
                Users.AddRange((IEnumerable<User>)xmlSer.Deserialize(xr));
            }
            Users.Add(new User(username, HashPassword(password)));
            using (var xw = XmlWriter.Create(Path))
            {
                xmlSer.Serialize(xw, Users);
            }
            var user = GetUser(username);
            return user != null;
        }

        public static bool Login(string username, string password)
        {
            Users.Clear();
            var xmlSer = new XmlSerializer(typeof(List<User>));
            using (var xr = XmlReader.Create(Path))
            {
                Users.AddRange((IEnumerable<User>)xmlSer.Deserialize(xr));
            }
            var user = Users.FirstOrDefault(n => n.Username == username);
            if (user == null) return false;
            CheckPassword(user.PasswordHash, password);
            return true;
        }

        public static bool DeleteUser(string username, string password)
        {
            Users.Clear();
            var xmlSer = new XmlSerializer(typeof(List<User>));
            using (var xr = XmlReader.Create(Path))
            {
                Users.AddRange((IEnumerable<User>)xmlSer.Deserialize(xr));
            }
            var user = Users.FirstOrDefault(n => n.Username == username);
            CheckPassword(user.PasswordHash, password);
            Users.Remove(user);
            using (var xw = XmlWriter.Create(Path))
            {
                xmlSer.Serialize(xw, Users);
            }
            user = GetUser(username);
            return user == null;
        }

        public static User GetUser(string username)
        {
            Users.Clear();
            var xmlSer = new XmlSerializer(typeof(List<User>));
            using (var xr = XmlReader.Create(Path))
            {
                Users.AddRange((IEnumerable<User>)xmlSer.Deserialize(xr));
            }
            return Users.FirstOrDefault(n => n.Username == username);
        }

        public static string HashPassword(string password)
        {
            //Create the salt value with a cryptographic PRNG
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            //Create the Rfc2898DeriveBytes and get the hash value
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            //Combine the salt and password bytes for later use
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            //Turn the combined salt+hash into a string for storage
            return Convert.ToBase64String(hashBytes);
        }

        public static void CheckPassword(string passwordHash, string password)
        {
            //Extract the bytes
            byte[] hashBytes = Convert.FromBase64String(passwordHash);
            //Get the salt
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            //Compute the hash on the password the user entered
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            //Compare the results
            for (var i = 0; i < 20; i++)
            {
                if (hashBytes[i + 16] != hash[i])
                {
                    throw new UnauthorizedAccessException();
                }
            }
        }
    }
}