﻿using DAA.PE.TDD;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class AuthenticationTest
    {
        [TestMethod]
        public void CreateUserTest()
        {
            var username = "TestUser1";
            var password = "password1";
            var result = Authentication.CreateUser(username, password);
            Assert.IsTrue(result, "User creation failed");
        }

        [TestMethod]
        public void LoginTest()
        {
            var username = "TestUser1";
            var password = "password1";
            var user = Authentication.GetUser(username);
            var result = Authentication.Login(username, password);
            Assert.IsNotNull(user, "User is not found");
            Assert.IsTrue(result, "User login failed");
        }

        [TestMethod]
        public void DeleteUserTest()
        {
            var username = "TestUser1";
            var password = "password1";
            var result = Authentication.DeleteUser(username, password);
            Assert.IsTrue(result, "User delete failed");
        }
    }
}